# Vue Storyblok UI

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve:field-sp-route
```

### Compiles and minifies for production
```
npm run build:field-sp-route
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
