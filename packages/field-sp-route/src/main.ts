import Plugin from './Plugin.vue';
import { setupPlugin } from '@vue-storyblok-ui/common';

setupPlugin(Plugin);
