import Vue, { VueConstructor } from 'vue';

declare global {
  interface Window {
    Fieldtype: StoryblokPlugin;
    Storyblok: Storyblok;
    storyblok: storyblok;
    StoryblokPluginRegistered: boolean;
  }
}

export interface Storyblok {
  vue: typeof Vue;
  plugin: typeof Vue;
}

export interface storyblok {
  field_types: Record<string, typeof Vue>;
}

export interface StoryblokPluginModel {
  [key: string]: boolean | number | string | [] | Record<string | number, unknown>;
  plugin: string;
}

export type StoryblokPlugin = VueConstructor<
  Record<string, any> & { name: string; initWith: () => StoryblokPluginModel } & Vue
>;

export function isStoryblokHostedWindow(wnd: Window): boolean {
  if (process.env.NODE_ENV === 'development') {
    return 'Storyblok' in wnd && 'vue' in wnd.Storyblok;
  }

  return true;
}

export function setupPlugin(Component: StoryblokPlugin): void {
  if (!isStoryblokHostedWindow(window)) {
    throw new Error('Invalid Storyblok plugin editor environment');
  }

  if (process.env.NODE_ENV === 'development') {
    window.Fieldtype = Component;
    window.Storyblok.vue.component('custom-plugin', window.Storyblok.vue.extend(window.Fieldtype));
    window.StoryblokPluginRegistered = true;
  } else {
    window.storyblok.field_types[Component.name] = Component;
  }
}
