import Vue from 'vue';
import { ExtendedVue } from 'vue/types/vue';
import { ThisTypedComponentOptionsWithRecordProps } from 'vue/types/options';

export function defineComponent<Data, Methods, Computed, Props, V = Vue>(
  options: ThisTypedComponentOptionsWithRecordProps<V & Vue, Data, Methods, Computed, Props>,
  meta: V
): ExtendedVue<V & Vue, Data, Methods, Computed, Props>;
export function defineComponent(options: unknown, _meta: unknown): unknown {
  return options;
}
